import logging

log = logging.getLogger('djpagseguro.checkout')


class Checkout(object):

    currency = 'BRL'
    email = None
    items = None
    reference = None
    sender_name = None
    sender_email = None
    sender_areacode = None
    sender_phone = None
    token = None

    __required = (
        'currency',
        'email',
        'items',
        'token',
    )

    def __init__(self, *args, **kwargs):
        for attr, val in kwargs.items():
            setattr(self, attr, val)

    def __setattr__(self, attr, value):
        if not hasattr(self, attr):
            raise ValueError(attr)
        object.__setattr__(self, attr, value)

    def __iter__(self):

        self.is_valid()

        data = {
            'currency': self.currency,
            'token': self.token,
            'email': self.email,
            'reference': self.reference,
        }

        for i, item in enumerate(self.items):
            data.update(item.to_dict(i + 1))
        return iter(data.items())

    def is_valid(self):
        for attr in self.__required:
            if getattr(self, attr) is None:
                raise ValueError(attr)

    def add_item(self, i):
        if not isinstance(i, Item):
            raise ValueError(i)
        if self.items is None:
            self.items = []
        self.items.append(i)


class Item(object):

    amount = None
    id = None
    description = None
    quantity = None
    shipping_cost = None
    weight = None

    def to_dict(self, i):
        data = {
            'itemId%d' % i: self.id,
            'itemDescription%d' % i: self.description,
            'itemAmount%d' % i: self.amount,
            'itemQuantity%d' % i: self.quantity,
            #'itemWeight%d' % i: self.weight,
        }
        return data
