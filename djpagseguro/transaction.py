from decimal import Decimal
from lxml import etree
import logging

log = logging.getLogger('djpagseguro.transaction')


class Transaction(object):

    currency = 'BRL'
    code = None
    discount_amount = None
    extra_acmount = None
    fee_amount = None
    gross_amount = None
    items = None
    net_amount = None
    reference = None
    sender_name = None
    sender_email = None
    sender_areacode = None
    sender_phone = None
    status = None
    token = None
    type = None

    @classmethod
    def from_xml(cls, data):
        xml = etree.fromstring(data)
        obj = Transaction()
        obj.code = xml.xpath("//transaction/code")[0].text
        ref = xml.xpath("//transaction/reference")
        if ref:
            obj.reference = xml.xpath("//transaction/reference")[0].text
        obj.type = int(xml.xpath("//transaction/type")[0].text)
        obj.status = int(xml.xpath("//transaction/status")[0].text)
        obj.gross_amount = Decimal(
            xml.xpath("//transaction/grossAmount")[0].text
        )
        try:
            obj.sender_name = xml.xpath("//transaction/sender/name")[0].text
        except Exception as e:
            #conta de teste do pagseguro nao contem nome
            log.info("erro ao obter o nome no envio "+str(e))
            obj.sender_name = "Nome Não Identificado"
        obj.sender_email = xml.xpath("//transaction/sender/email")[0].text
        return obj

    def __init__(self, *args, **kwargs):
        for attr, val in kwargs.items():
            setattr(self, attr, val)

    def __setattr__(self, attr, value):
        if not hasattr(self, attr):
            raise ValueError(attr)
        object.__setattr__(self, attr, value)

    def __repr__(self):
        return '<Transaction %s>' % self.code

    def __iter__(self):
        for attr in dir(self):
            if attr.startswith('_'):
                continue
            val = getattr(self, attr)
            if callable(val):
                continue
            yield (attr, val)

    def add_item(self, i):
        if not isinstance(i, Item):
            raise ValueError(i)
        if self.items is None:
            self.items = []
        self.items.append(i)


class Item(object):

    amount = None
    id = None
    description = None
    quantity = None
    shipping_cost = None
    weight = None

    def to_dict(self, i):
        data = {
            'itemId%d' % i: self.id,
            'itemDescription%d' % i: self.description,
            'itemAmount%d' % i: self.amount,
            'itemQuantity%d' % i: self.quantity,
            #'itemWeight%d' % i: self.weight,
        }
        return data
