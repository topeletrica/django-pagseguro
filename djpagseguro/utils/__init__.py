from lxml import etree
import logging
from django.conf import settings

from djpagseguro.transaction import Transaction

import requests

CHECKOUT_URL = 'https://ws.pagseguro.uol.com.br/v2/checkout/'
TRANSACTION_URL = (
    'https://ws.pagseguro.uol.com.br/v2/transactions/notifications'
)

if getattr(settings, 'PAGSEGURO_SANDBOX', True):
    CHECKOUT_URL = 'https://ws.sandbox.pagseguro.uol.com.br/v2/checkout/'
    TRANSACTION_URL = (
        'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/notifications'
    )


log = logging.getLogger('djpagseguro.utils')




def new_payment(checkout):

    r = requests.post(CHECKOUT_URL, data=dict(checkout), headers={
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    })
    response = etree.fromstring(r.content)

    #print etree.tostring(response, pretty_print=True)
    error = response.xpath("//error/message")
    if error:
        raise ValueError(error[0].text)

    code = response.xpath("//checkout/code")
    if not code:
        raise ValueError("No code")

    return code[0].text


def get_transaction(code, email, token):

    r = requests.get(
        "%s/%s" % (TRANSACTION_URL, code),
        params={
            'email': email,
            'token': token,
        },
    )
    if r.status_code != 200:
        raise ValueError("code: %s / email: %s / token: %s / status: %s /invalid request" % (code, email,token, r.status_code))

    return Transaction.from_xml(r.content)
