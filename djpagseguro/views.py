import logging

from django.conf import settings
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from djpagseguro import signals
from djpagseguro.utils import get_transaction

log = logging.getLogger('djpagseguro.views')


@csrf_exempt
def notification(request):

    if request.method != 'POST':
        log.warn('Received a request != POST, aborting!')
        return HttpResponse()

    log.debug('POST data: %r', request.POST)
    code = request.POST.get('notificationCode')
    ttype = request.POST.get('notificationType')

    if ttype != 'transaction':
        return HttpResponse()

    email = settings.PAGSEGURO_EMAIL
    token = settings.PAGSEGURO_TOKEN

    transaction = get_transaction(code, email, token)
    log.debug('Got transaction %r', transaction)
    signals.notification.send(sender=transaction)
    return HttpResponse()
